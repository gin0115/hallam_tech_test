<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bitbucket.org/gin0115/
 * @since      1.0.0
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/public
 * @author     Glynn Quelch <glynn.quelch@gmail.com>
 */
class Hallam_tech_test_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}



	/**
	 * Pre populate the hidden fields with the page/post ID and Title
	 *
	 * @since    1.0.0
	 */
	public function pre_populate_test_form($form) {

		//Check the form has fields
		if (is_array($form['fields'])) {
			//Loop through and look for our Parameter Names
			foreach ($form['fields'] as $fieldKey => $field) {
				//Post Title
				if ($field->inputName == 'postTitle') {
					//Update the default value with the page title
					$form['fields'][$fieldKey]->defaultValue = get_the_title();
				}

				//Post ID
				if ($field->inputName == 'postID') {
					//Update the default value with the page title
					$form['fields'][$fieldKey]->defaultValue = get_the_ID();
				}
			} //End foreach

		}

		return $form;
	}

}
