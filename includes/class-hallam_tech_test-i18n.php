<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bitbucket.org/gin0115/
 * @since      1.0.0
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/includes
 * @author     Glynn Quelch <glynn.quelch@gmail.com>
 */
class Hallam_tech_test_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'hallam_tech_test',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
