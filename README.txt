=== Plugin Name ===
Contributors: Glynn Quelch
Donate link: https://bitbucket.org/gin0115/
Tags: comments, spam
Requires at least: 3.9.8
Tested up to: 3.9.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This is a test plugin created as part of a job application for Hallam. 
