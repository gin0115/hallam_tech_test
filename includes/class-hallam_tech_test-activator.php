<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/gin0115/
 * @since      1.0.0
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/includes
 * @author     Glynn Quelch <glynn.quelch@gmail.com>
 */
class Hallam_tech_test_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		//Create auto removal frequency
		add_option( 'auto_remove_entries', 'off');
	}

}
