<?php
/**
 * Custom Gravity Form view of all forms with controls to delete entires
 *
 * @since    1.0.0
 */

  //Get our form data
  $testListTable = new Form_Entries_Table();
  $testListTable->prepare_items();

  //If the auto update frequency has been changed, pass the $_REQUEST to update the option
  if (isset($_REQUEST['update_auto_delete'])) {
    $testListTable->auto_delete_entries_frequency_update($_REQUEST);
  }

  //Get the current auto-delete (fallback to all if not set)
  $autoDeleteFrequency = get_option('auto_remove_entries', 'off');
  $autoDeleteFrequencyOptions = $testListTable->auto_delete_entries_frequency_options();
  ?>


  <div class="wrap">

    <div id="icon-users" class="icon32"><br/></div>
    <h2>Delete Form Entries</h2>
    <p>Please select which forms you wish to delete entries for. You can specify which entries to delete, by using the drop down box provided.</p>

    <div class="auto_delete_settings" style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
      <h3>Auto Entry Removal</h3>
      <p>Old entries can be automatically deleted after a specified time. </p>
      <p>Current Setting : <span class="current-setting" style="color: green;font-weight: 700;margin-right: 5px;"><?php echo esc_html( Form_Entries_Table::auto_delete_entries_frequency_parse_display($autoDeleteFrequency ) ) ;?></span> <span class="button toggle">Change</span> </p>

      <div class="change-auto-delete-frequency" style="display: none;">
        <p>Update your auto delete frequency</p>
        <form id="auto-delete" method="get" style="padding-bottom: 10px;">

          <input type="hidden" name="page" value="delete_form_entries">
          <?php wp_nonce_field( 'auto_delete_entries_frequency', '_wpnonce' ); ?>
          <select class="" name="auto_delete_frequency">
            <?php foreach ($autoDeleteFrequencyOptions as $key => $value): ?>
              <option value="<?php echo esc_html($key); ?>" <?php if($key == $autoDeleteFrequency){echo 'selected';}?>><?php echo esc_html($value); ?></option>
            <?php endforeach; ?>
          </select>
          <input type="submit" name="update_auto_delete" value="Update" class="button">
          <button type="button" name="cancel_auto_delete" class="button update_auto_delete">Cancel</button>

        </form>
      </div>

    </div>

    <form id="forms-filter" method="get">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <?php $testListTable->display(); ?>
    </form>

  </div>
  <?php
