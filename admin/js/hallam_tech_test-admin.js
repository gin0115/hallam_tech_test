jQuery( function($) {
	//Add confirmation prompt on using either bulk action or link click
	$('#forms-filter .delete a, #forms-filter input.button').click( function( event ) {
			if( ! confirm( 'Are you sure you want to delete these entries?' ) ) {
					event.preventDefault();
			}
	});

	//Show update frequency dropdown on click
	$('.auto_delete_settings span.toggle').click( function() {
			$('.change-auto-delete-frequency').show();
	});

	//Cancel/Hide frequency dropdown on click
	$('.auto_delete_settings button.update_auto_delete').click( function() {
			$('.change-auto-delete-frequency').hide();
	});


});
