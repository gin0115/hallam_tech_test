<?php
/**
 * Creates the List Table to display the forms and process the actions
 *
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/admin
 * @author     Glynn Quelch <glynn.quelch@gmail.com>
 */

 /**
  * Load list table class to extend.
  *
  * @since    1.0.0
  */
if(!class_exists('WP_List_Table')){

	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

}



/**
 * Extend the WP List Table to be used with this plugin
 *
 * @since    1.0.0
 */
class Form_Entries_Table extends WP_List_Table {

	public function __construct() {

		parent::__construct(
			array(
				'singular'  => 'form',
				'plural'    => 'forms',
				'ajax'      => false
			)
		);
	}


/**
 * Define the columns headers
 *
 * @since    1.0.0
 */
public function column_title($item){

	//Build row actions
	$actions = array(
		'view'      => sprintf('<a href="?page=%s&id=%s">View Entries</a>',
			'gf_entries',
			$item['id']),
		'delete'    => sprintf('<a href="?page=%s&action=%s&_wpnonce=%s&form[0]=%s">Delete All Entries</a>',
			$_REQUEST['page'],
			'delete_all_single',
			wp_create_nonce( 'delete-single' ),
			$item['id']),
	);

	//Return the title contents
	return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
		$item['title'],
		$item['id'],
		$this->row_actions($actions)
	);

}


/**
 * Add in our count column
 *
 * @since    1.0.0
 */
public function column_default($item, $column_name){

	switch($column_name){
		case 'entries':
			return $item[$column_name];

		default:
			return print_r($item,true);
	}

}


/**
 * Display Checkbox on table
 *
 * @since    1.0.0
 */
public function column_cb($item){

	return sprintf(
		'<input type="checkbox" name="%1$s[]" value="%2$s" />',
		$this->_args['singular'],
		$item['id']
	);

}


/**
 * Define the columns used in the table
 *
	 * @since    1.0.0
 */
public function get_columns(){

	$columns = array(
		'cb'        => '<input type="checkbox" />',
		'title'     => 'Form Title',
		'entries'    => 'Total Entries',
	);

	return $columns;

}


/**
 * Define Bulk Actions
 *
 * @since    1.0.0
 */
public function get_bulk_actions() {

	$actions = array(
		'delete_1day'    => 'Delete all entries for selected form(s) over 1 day old',
		'delete_1week'    => 'Delete all entries for selected form(s) over 1 week old',
		'delete_1month'    => 'Delete all entries for selected form(s) over 1 month old',
		'delete_1year'    => 'Delete all entries for selected form(s) over 1 year old',
		'delete_all'    => 'Delete all entries for selected form(s)',
	);

	return $actions;

}


/**
 * Delete selected Entries
 *
 * @since    1.0.0
 */
public function process_bulk_action() {

	$action = $this->current_action() ;

	//If no action is defined, abort
	if (!$action) {
		return;
	}

	//Define the nonce to check based on the action
  switch ($action) {
    case 'delete_all_single':
      $nonceAction = 'delete-single';
    break;

    default:
      $nonceAction = 'bulk-' . $this->_args['plural'];
    break;
  }

	//Check the nonce
	if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], $nonceAction ) ) {
		die( 'Something went wrong.' );
	}


	//Run the actions
	switch ($action) {

		case 'delete_1day':
			$this->delete_form_entries_ranged( $_GET['form'], '-1 day');
		break;

		case 'delete_1week':
			$this->delete_form_entries_ranged( $_GET['form'], '-1 week');
		break;

		case 'delete_1month':
			$this->delete_form_entries_ranged( $_GET['form'], '-1 month');
		break;

		case 'delete_1year':
			$this->delete_form_entries_ranged( $_GET['form'], '-1 year');
		break;

		case 'delete_all':
		case 'delete_all_single':
			$this->delete_form_entries_ranged( $_GET['form'], '+12 hours');
		break;



		default:
			// Do noting
		break;
	}


}


/**
 * Return an array of Form Ids
 *
 * @since    1.0.0
 */
public function get_form_id_array() {

   $returnArray = array();

   //Get the forms
   $forms = GFAPI::get_forms();

   //Loop through and return the IDs
   if (is_array($forms) && count($forms) > 0) {
     foreach ($forms as $form) {
       $returnArray[] = $form['id'];
     }
   }

   return $returnArray;
}


/**
 * Delete all entries based on date range
 *
 * @since    1.0.0
 */
public function delete_form_entries_ranged($formIDs, $range) {

	//If we do not have an array of IDs, abort
	if (!is_array($formIDs)) {
		return;
	}

	//Get the entries for the forms by looping through
	foreach ($formIDs as $formID) {

		$entries = GFAPI::get_entries( esc_attr( $formID ) );
		//Check we have entries
		if (is_array($entries) && count($entries) > 0) {
			//Loop through the entries and delete them
			foreach ($entries as $entry) {
				//Check if the entry is older than the date range specified.
				if (strtotime( $entry['date_created']) <= strtotime($range)) {
					//echo strtotime ( $entry['date_created']).'<hr>';
					GFAPI::delete_entry( $entry['id'] );
				}
			}

		}//end if

	}//end foreach

}


/**
 * Get a list of all forms and entry comments
 *
 * @since    1.0.0
 */
public function get_gform_list() {

	//Return back
	$forms = GFAPI::get_forms();

	//If we have forms, count up the number of entries
	if (is_array($forms) && count($forms) > 0) {
		foreach ($forms as $key => $form) {
			///Get the entry count
			$forms[$key]['entries'] = $this->count_up_form_entries( $form['id'] );
		}
	}

	//Return the processed form list
	return $forms;

}

/**
 * Count up the number of form entries which are not marked as trash
 *
 * @since    1.0.0
 */
public function count_up_form_entries($formID){

	//Get the entries from the form ID
	$entries = GFAPI::get_entries($formID);
	$count = 0;

	//Loop through the entires and count up how many are "active"
	foreach ($entries as $entry) {
		if ($entry['status'] == 'active') {
			$count++;
		}
	}

	//Return the count
	return $count;

}


/**
 * Prepare the rows for our table
 *
 * @since    1.0.0
 */
public function prepare_items() {

	//Records per page
	$per_page = 10;
	//Column Headers
	$columns = $this->get_columns();
	$this->_column_headers = array($columns, array(), array());
	//Process Bulk Actions
	$this->process_bulk_action();
	//Get forms data to populate the table
	$data = $this->get_gform_list();

	//Pagination Args
	$current_page = $this->get_pagenum();
	$total_items = count($data);
	$this->set_pagination_args(
		array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => ceil($total_items/$per_page)
		)
	);

	//Slice array based on current page
	$data = array_slice($data,(($current_page-1)*$per_page),$per_page);

	//Define our data for the table
	$this->items = $data;

}


/**
 * Auto Delete Freuqncy values
 *
 * @since    1.0.0
 */
public function auto_delete_entries_frequency_options() {
  return array(
    'off' => 'Turned Off',
    '-1 day' => 'Older than 1 day',
    '-1 week' => 'Older than 1 week',
    '-1 month' => 'Older than 1 month',
    '-1 year' => 'Older than 1 year',
  );
}


/**
 * Auto Delete Freuqncy values
 *
 * @since    1.0.0
 */
public function auto_delete_entries_frequency_update($request) {

  //Check the nonce
	if ( ! wp_verify_nonce( $request['_wpnonce'], 'auto_delete_entries_frequency' ) ) {
		die( 'Something went wrong.' );
	}

  //If frquency is defined, update option
  if (isset($request['auto_delete_frequency'])) {
    update_option('auto_remove_entries', sanitize_text_field($request['auto_delete_frequency']));
  }
}

/**
 * Auto Delete Freuqncy values
 *
 * @since    1.0.0
 */
public function auto_delete_entries_frequency_parse_display($value) {

  //Get the values
	$options = Form_Entries_Table::auto_delete_entries_frequency_options();
  //Loop through and return the displayed value
  foreach ($options as $key => $option) {
    if ($key == $value) {
      return $option;
    }
  }
}

}
