<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/gin0115/
 * @since      1.0.0
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/admin
 */


 /**
  * Register the wp-cron hook to handle the automated deletion of entries.
  */
  register_activation_hook( __FILE__, 'do_auto_delete_entries_hook' );

	
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hallam_tech_test
 * @subpackage Hallam_tech_test/admin
 * @author     Glynn Quelch <glynn.quelch@gmail.com>
 */
class Hallam_tech_test_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}



	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/hallam_tech_test-admin.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Add submenu page to Gravity forms
	 *
	 * @since    1.0.0
	 */
	public function create_gf_submenu( $menus ) {

	  $menus[] = array(
			'name' => 'delete_form_entries',
			'label' => __( 'Delete Form Entries' ),
			'callback' =>  array($this, 'delete_form_entries_view')
		);

		return $menus;
	}


	/**
	 * Display the view
	 *
	 * @since    1.0.0
	 */
	public function delete_form_entries_view() {

		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		include_once dirname( __FILE__ )  . '/views/hallam_tech_test-admin-display.php';

	}

	/**
	 * Schedule auto entry delete
	 *
	 * @since    1.0.0
	 */

	public function auto_delete_entries_cron_job() {

		//Check if we are ready to run the cron job and run hourly.
		if ( ! wp_next_scheduled( 'do_auto_delete_entries_hook' ) ) {
			wp_schedule_event( time(), 'hourly', 'do_auto_delete_entries_hook' );
	  }

	}


	/**
	 * Run sheduled task
	 *
	 * @since    1.0.0
	 */
	public function do_auto_delete_entries() {

		//Get the date range of entries to remove
		$range = sanitize_text_field( get_option('auto_remove_entries') );

		//If auto delete is turned off, escape
		if ($range == 'off') {
			return;
		}

		//Collect an array of all the form ids
		$forms = Form_Entries_Table::get_form_id_array();

		//Delete where appropiate.
		Form_Entries_Table::delete_form_entries_ranged($forms, $range);

	}

}
